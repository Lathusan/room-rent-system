package com.parent;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 1, 2021
 **/

public interface RentedRoom {

double baseFees=1000;

default double baseFees() {
	return baseFees ;}

public default double getCost() {
	return baseFees;}
}


