package com.demo;

import java.util.Scanner;

import com.child.LuxuryRoom;
import com.child.NormalRoom;
import com.grantchild.SemeLuxuryRoom;
import com.grantchild.SupeerLuxuryRoom;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 1, 2021
 **/

public class RoomDemo {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int input;

		System.out.println("Welcome to the Room Rental System");
		System.out.println("=================================\n");

		System.out.println("1. Room Rent");
		System.out.println("6. Exit");
		System.out.print("Select one : ");
		input = scan.nextInt();
		
System.exit(0);
		if (input == 1) {
			LuxuryRoom luxuryRoom = new LuxuryRoom();
			System.out.println("\n2. Luxury Room");
			System.out.println("3. Normal Room");
			System.out.print("Select one : ");
			input = scan.nextInt();
			
			if (input == 2) {
				System.out.println("\n4. SemeLuxury Room");
				System.out.println("5. SuperLuxury Room");
				System.out.print("Select one : ");
				input = scan.nextInt();
				
				if (input == 4) {
					SemeLuxuryRoom semeLuxuryRoom = new SemeLuxuryRoom();
					semeLuxuryRoom.baseFees();
					luxuryRoom.getCost();
					
				} else if (input == 5) {
					SupeerLuxuryRoom supeerLuxuryRoom = new SupeerLuxuryRoom();
					supeerLuxuryRoom.baseFees();
					luxuryRoom.getCost();
				}
			} else if (input == 3) {
				NormalRoom normalRoom = new NormalRoom();
				normalRoom.baseFees();
				luxuryRoom.getCost();
			}
		} else {
			System.out.println("Good Bye !!!");
			System.exit(0);
		}

	}

}
