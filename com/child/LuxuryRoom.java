package com.child;

import java.util.Scanner;

import com.parent.RentedRoom;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 1, 2021
 **/

public class LuxuryRoom implements RentedRoom {
	private int numberOfDays;
	int total;
	Scanner sc = new Scanner(System.in);
	int input = sc.nextInt();

	int numberOfDays() {
		return numberOfDays;

	}

	double getRentalFees() {

		if (input < 5) {
			total = 100 * input;

		} else if (input >= 5 || input <= 10) {
			total = 100 * input;
		} else {
			total = 50 * input;
		}
		return total;

	}

}
